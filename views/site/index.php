<?php
use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Consultas de Seleccion 1</h1>

        <p class="lead">Modulo 3 - Unidad 2 Framework Yii2</p>
    </div>

    <div class="body-content">
        <div class="row">
              <!-- 
            boton de consulta0a
            -->
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 0</h3>
                        <p>Listar los ciclistas (sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta0a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta0'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
            <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 1</h3>
                        <p>Listar las edades de los ciclistas (sin repetidos)</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta1a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta1'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
            <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 2</h3>
                        <p>Listar las edades de los ciclistas de Artiach</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta2a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta2'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
             <!-- 
            boton de consulta
            -->
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 3</h3>
                        <p>Listar las edades de los ciclistas de Artiach o de Amore Vita</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta3a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta3'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
               <!-- 
            boton de consulta0a
            -->
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 4</h3>
                        <p>Listar los ciclistas manores de 25 y amayores de 30</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta4a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta4'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
                 <!-- 
            boton de consulta0a
            -->
            <div class="col-sm-6 col-md-3">
                <div class="thumbnail">
                    <div class="caption">
                        <h3>Consulta 5</h3>
                        <p>Listar los ciclistas manores de 25 y amayores de 30</p>
                        <p>
                            <?= Html::a('Active Record',['site/consulta5a'],['class' => 'btn btn-primary']); ?>
                            <?= Html::a('DAO',['site/consulta5'],['class' => 'btn btn-default']); ?>
                        </p>
                    </div>
                </div>
            </div>
            <!--
            fin de boton de consulta
            -->
        </div>
    </div>
</div>
