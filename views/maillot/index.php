<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Maillots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="maillot-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Maillot', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'código',
            'tipo',
            'color',
            'premio',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
